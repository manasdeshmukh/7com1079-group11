library(tidyverse)
d <- read_csv("CO2 Emissions_Canada.csv")
pdf("visualization.pdf")
x <- d$`Engine Size(L)`
y <- d$`CO2 Emissions(g/km)`
plot(x,y,main = "Engine Size of Vehicles vs Carbon Dioxide Emissions",xlab = "Engine Size (in Litres)",ylab = "CO2 Emissions (in g/km)",pch = 19,frame = T)
model <- lm(y~x, data = d)
abline(model,col = "blue")
yax <- d$`CO2 Emissions(g/km)`
h <- hist(yax,6,main = "CO2 Emissions from Vehicles",xlab = "CO2 Emissions (in g/km)",ylab = "Number of Vehicles",col = "azure")
mn <- mean(yax)
stdD <- sd(yax)
xax <- seq(100,700,1)
y1 <- dnorm(xax,mean = mn,sd = stdD)
y1 <- y1 * diff(h$mids[1:2] * length(yax))
lines(xax,y1,col = "blue")
dev.off()